<?php

// src/AppBundle/Controller/CategoryController.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use FOS\UserBundle\Controller\RegistrationController as RegistrationController;
use AppBundle\Form\RegisterEmailType;
use AppBundle\Form\RegisterPhoneType;
use AppBundle\Form\RegisterPasswordType;
use AppBundle\Services\Mailer\Mailer;
use AppBundle\Services\Registration\RegistrationService;

/**
 * Class related to Controller category and used 
 * for routes with /register prefix
 *
 * @category Controller
 * @package   
 * @author    
 * @license  
 * @link     
 */
class RegisterController extends RegistrationController 
{
    /**
     * @Route("/register/", name="registration_step_one")
     */
    public function choiceRegistrationAction(
        Request $request,  
        \Swift_Mailer $mailer, 
        Session $session
    ) {   
        $options = [];
        $formEmail = $this->createForm(RegisterEmailType::class, $options);
        $formEmail->handleRequest($request);
        if ($formEmail->isSubmitted() && $formEmail->isValid()) {
            $code = rand(100000, 999999);
            $data = $formEmail->getData();
            if ($this->ifEmailExistAction($data['email'], $session)) {
                return $this->redirectToRoute('registration_step_one');
            }
            $body = $this->renderView(
                'emails/registrationCode.html.twig', 
                [
                    'code' => $code,
                ]
            );
            $emailSend = new Mailer($data['email'], $body);
            $emailSend->send($mailer);
            $session->set('code', $code);
            $session->set('email', $data['email']);
            return $this->redirectToRoute('registration_step_two');
        }
        $formPhone = $this->createForm(RegisterPhoneType::class, $options);
        $formPhone->handleRequest($request);
        if ($formPhone->isSubmitted() && $formPhone->isValid()) {
            $code = rand(100000, 999999);
            $data = $formPhone->getData();
            if ($this->ifPhoneExistAction($data['phone'], $session)) {
                return $this->redirectToRoute('registration_step_one');
            }
            $smsSender = $this->get('sms_sender');
            if (!$smsSender->send($code, $data['phone'])) {
                $session->set(
                    'register_errors', 
                    'Извините, но в данный момент регистрация через телефон невозможна'
                );
                return $this->redirectToRoute('registration_step_one');
            }
            $session->set('code', $code);
            $session->set('phone', $data['phone']);
            return $this->redirectToRoute('registration_step_two');
        }
        return $this->render(
            'registration/stepOne.html.twig', 
            [
                'formEmail' => $formEmail->createView(),
                'formPhone' => $formPhone->createView(),
                'errors' => $session->get('register_errors'),
            ]
        );

    }

    /**
     * @Route("/register/stepTwo", name="registration_step_two")
     */
    public function passwordAction(
        Request $request, 
        Session $session, 
        EntityManagerInterface $em
    ) {   
        $options = [];
        $session->remove('register_errors');
        $formPassword = $this->createForm(RegisterPasswordType::class, $options);
        $formPassword->handleRequest($request);
        if ($formPassword->isSubmitted() && $formPassword->isValid()) {
            $data = $formPassword->getData();
            if ($data['code'] != $session->get('code')) {
                $session->set('code_errors', 'Неправильно введен код');
                return $this->redirectToRoute('registration_step_two');
            }
            $userManager = $this->get('fos_user.user_manager');
            $registerService = new RegistrationService($session, $em, $data, $userManager); 
            if ($registerService->createUser()) {
                return $this->redirectToRoute('registration_step_three');
            }
            return new Response('Извините регистрация в данный момент невозвожна');
        }
        return $this->render(
            'registration/stepTwo.html.twig', 
            [
                'formPassword' => $formPassword->createView(),
                'errors' => $session->get('code_errors'),
            ]
        );

    }

    /**
     * @Route("/register/succes", name="registration_step_three")
     */
    public function registrationSuccessAction()
    {  
        return $this->render('registration/success.html.twig');
    }

    /**
     * Function that checked presence of user email in the DB
     * 
     * @param  string  $email   <p>Email of user</p>
     * @param  Session $session <p>Session object</p>
     * @return boolean          <p>True if email presents false if isn't</p>
     */
    protected function ifEmailExistAction($email, Session $session) 
    {
        $user = $this->get('fos_user.user_manager')->findUserByEmail($email);
        if ($user !== null) {
            $session->set('register_errors', 'Данный email уже используется');
            return true;
        }
        return false;
    }
    
    /**
     * Function that checked presence of user phonenumber in the DB
     * 
     * @param  string  $phone   <p>Phone number of user</p>
     * @param  Session $session <p>Session object</p>
     * @return boolean         <p>True if phone number presents false if isn't</p>
     */
    protected function ifPhoneExistAction($phone, Session $session) 
    {
        $user = $this->get('fos_user.user_manager')->findUserByUsername($phone);
        if ($user !== null) {
            $session->set('register_errors', 'Данный телефон уже используется');
            return true;
        }
        return false;
    }
}
