<?php

// src/AppBundle/Controller/SiteController.php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class related to Controller category and used for homepage route
 *
 * @category Controller
 * @package   
 * @author    
 * @license  
 * @link     
 */
class SiteController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('fos_user_profile_show');
        }
        return $this->redirectToRoute('fos_user_security_login');
    }
}
