<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Class that could be used check balance of bitcoin
 * wallet by console command
 *
 * @category CommandClass
 * @package   
 * @author    
 * @license  
 * @link     
 */
class BitcoinBalanceCommand extends ContainerAwareCommand
{
    /**
     * Function that configure console command
     * 
     * @return void
     */
    protected function configure()
    {
        $this->setName('bitcoin:balance')
            ->setDescription('Show wallet balance')
            ->setHelp('This command display balance of the bitcoin wallet');
    }
    
    /**
     * Function that execute console command
     * 
     * @param  InputInterface  $input  <p>InputInterface object</p>
     * @param  OutputInterface $output <p>OutputInterface object</p>
     * @return integer                 <p>0 if ballance was showed or 
     *                                    code of error if it peresented</p>
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        $bitcoin = $this->getContainer()->get('bitcoin_service');
        $balance = $bitcoin->getBalance();
        if (!$balance['result']) {
            $output->writeln($balance['error']['message']);
            return $balance['error']['code'];
        }
        $output->writeln('The balance of the wallet is '.$balance['result']);
        return 0;
    }
}
