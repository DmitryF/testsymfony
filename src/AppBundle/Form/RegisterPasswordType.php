<?php

namespace AppBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class that could be used for building form 
 * with password input and code for actication
 *
 * @category AbstractTypeClass
 * @package   
 * @author    
 * @license  
 * @link     
 */
class RegisterPasswordType extends AbstractType
{
    /**
     * Function that builds form
     * 
     * @param FormBuilderInterface $builder 
     * @param array                $options Array with options (could be empty)
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'plainPassword', 
                LegacyFormHelper::getType(
                    'Symfony\Component\Form\Extension\Core\Type\RepeatedType'
                ), 
                [    
                    'type' => LegacyFormHelper::getType(
                        'Symfony\Component\Form\Extension\Core\Type\PasswordType'
                    ),
                    'options' => ['translation_domain' => 'FOSUserBundle'],
                    'first_options' => ['label' => 'Пароль'],
                    'second_options' => ['label' => 'Подтвердите пароль'],
                    'invalid_message' => 'fos_user.password.mismatch',
                ]
            )->add('code', TextType::class, ['label' => 'Введите код'])
            ->add('save', SubmitType::class, ['label' => 'Подтвердить']);
    }
}