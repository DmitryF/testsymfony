<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;

/**
 * Class that could be used for building form 
 * with user info 
 *
 * @category AbstractTypeClass
 * @package   
 * @author    
 * @license  
 * @link     
 */
class UserProfileInfoType extends AbstractType
{
    /**
     * Function that builds form
     * 
     * @param FormBuilderInterface $builder 
     * @param array                $options Array with options (could be Entity/User object)
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name', 
                TextType::class, 
                [
                    'label' => 'Укажите ваше имя',
                    'required' => false,
                ]
            )->add(
                'surname', 
                TextType::class, 
                [
                    'label' => 'Укажите вашу фамилию',
                    'required' => false,
                ]
            )->add(
                'country', 
                TextType::class, 
                [
                    'label' => 'Укажите вашу страну проживания',
                    'required' => false,
                ]
            )->add(
                'birthday', 
                BirthdayType::class, 
                [
                    'placeholder' => [
                        'year' => 'Год', 'month' => 'Месяц', 'day' => 'День',
                    ],
                    'required' => false,
                ]
            )->add(
                'sex', 
                ChoiceType::class, 
                [
                    'choices'  => [
                        'Выберите пол' => null, 
                        'Мужской' => 'Мужской',
                        'Женский' => 'Женский',
                    ],
                    'label' => 'Укажите пол',
                    'required' => false,
                ]
            )->add('save', SubmitType::class, ['label' => 'Подтвердить']);
    }
}