<?php

namespace AppBundle\Services\Bitcoin;

/**
 * Class that used for connection and sending commands to bitcoin server.
 * This class should be registered at services.yml file and its
 * parametes must be defined at parameter.yml file
 *
 * @category ServiceClass
 * @package   
 * @author    
 * @license  
 * @link     
 */
class BitcoinService
{
    /**
     * A bitcoin_user that defined in parameter.yml and should be similar
     * to rpcuser in the bitcoin.conf file
     */
    private $_rpcuser;
    
    /**
     * A bitcoin_password that defined in parameter.yml and should be similar
     * to rpcpassword in the bitcoin.conf file
     */
    private $_rpcpassword;

    /**
     * A bitcoin_port that defined in parameter.yml and should be similar
     * to rpcport in the bitcoin.conf file
     */
    private $_rpcport;
    
    /**
     * Initial function
     * 
     * @param  type $_rpcuser     Defined in parameter.yml
     * @param  type $_rpcpassword Defined in parameter.yml
     * @param  type $_rpcport     Defined in parameter.yml
     * @return void
     */
    public function __construct($_rpcuser, $_rpcpassword, $_rpcport) 
    {
        $this->_rpcuser = $_rpcuser;
        $this->_rpcpassword = $_rpcpassword;
        $this->_rpcport = $_rpcport;
    }
    
    /**
     * Function the create url for connection to bicoin server
     * 
     * @return string $url Url for curl for bitcoin server connection
     */
    protected function getConnection()
    {
        $url = "http://$this->_rpcuser:$this->_rpcpassword@127.0.0.1:$this->_rpcport";
        return $url;
    }

    /**
     * Function that initiate curl session for connection to bitcoin server
     * 
     * @param  string $post_string String with command for bitcoin server
     * @return object $curl
     */
    protected function init($post_string)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->getConnection());
        curl_setopt(
            $curl, 
            CURLOPT_HTTPHEADER, 
            [
                'Content-type: text/plain', 
                'Content-length: '.strlen($post_string)
            ]
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, 1);
        return $curl;
    }
    /**
     * Function that execute curl request and finish it session
     * 
     * @param  object $curl        <p>Curl object</p>
     * @param  string $post_string <p>String with command for bitcoin server</p>
     * @return string $output      <p>String with json encode result 
     *                                of curl execution</p>
     */
    protected function run($curl, $post_string)
    {
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_string);
        $output = curl_exec($curl);
        curl_close($curl);
        return $output;
    }
    
    /**
     * Function that get balance of the wallet from bitcoin server
     *  
     * @return array $balance <p>Array with balance of the wallet (if success)
     *                           or error description</p>
     */
    public function getBalance()
    {
        $post_string = '{"method": "getbalance", "params": [], "id": "balance"}';
        $curl = $this->init($post_string);
        $output = $this->run($curl, $post_string);
        $balance = json_decode($output, true);
        return $balance;
    }
    
    /**
     * Function that create new address for transaction on the bitcoin server
     *  
     * @return array $address <p>Array with created address (if success) 
     *                           or error description</p>
     */
    public function getNewAddress()
    {
        $post_string = '{"method": "getnewaddress", "params": [], "id": "address"}';
        $curl = $this->init($post_string);
        $output = $this->run($curl, $post_string);
        $address = json_decode($output, true);
        return $address;
    }
    
    /**
     * Function that can be used for making transaction on some address
     * 
     * @param  string $address <p>Address of bitcoin wallet</p>
     * @param  string $amount  <p>Amount of bitcoins for transaction</p>
     * @return array  $result  <p>Array with txid (if success) 
     *                            or error description</p>
     */
    public function sendToAddress($address, $amount)
    {
        $post_string = '{"method": "sendtoaddress", '
              .  '"params": ["'.$address.'", "'.$amount.'"],'
              .  '"id": "transaction"}';
        $curl = $this->init($post_string);
        $output = $this->run($curl, $post_string);
        $result = json_decode($output, true);
        return $result;

    }
}
