<?php

namespace AppBundle\Services\SMS;

/**
 * Class that used to send Sms using epochta.com.ua as sms port.
 * This class should be registered at services.yml file and its
 * parametes must be defined at parameter.yml file
 *
 * @category ServiceClass
 * @package   
 * @author    
 * @license  
 * @link     
 */
class SmsService
{

    /**
     * A epochta_user that defined in parameter.yml and should be similar
     * to your username in the epochta.com.ua service
     */
    private $_username;
    
    /**
     * A epochta_password that defined in parameter.yml and should be similar
     * to your password in the epochta.com.ua service
     */
    private $_password;

    /**
     * Initial function
     * 
     * @param  type $_username Defined in parameter.yml
     * @param  type $_password Defined in parameter.yml
     * @return void
     */
    public function __construct($_username, $_password) 
    {
        $this->_username = $_username;
        $this->_password = $_password;
    }
    
    /**
     * Function that creates message in xml format
     * 
     * @param  string $code        <p>Activation code</p>
     * @param  string $phoneNumber <p>Phone number of the user</p>
     * @return string $src         <p>Message that will be sent to api</p>
     */
    protected function createMessage($code, $phoneNumber)
    {
        $src = '<?xml version="1.0" encoding="UTF-8"?>    
                <SMS> 
                <operations>  
                <operation>SEND</operation> 
                </operations> 
                <authentification>    
                <username>' . $this->_username . '</username>   
                <password>' . $this->_password . '</password>   
                </authentification>   
                <message> 
                <sender>SMS</sender>    
                <text>Ваш код аткивации: '. $code . ' [UTF-8]</text>   
                </message>    
                <numbers> 
                <number>'. $phoneNumber .'</number>   
                </numbers>    
                </SMS>';  
        return $src;
    }
    
    /**
     * Function that create connection with sms port using curl
     * 
     * @param  string $code        <p>Activation code</p>
     * @param  string $phoneNumber <p>Phone number of the user</p>
     * @return xml    $output      <p>Answer from sms port site</p>
     */
    protected function run($code, $phoneNumber)
    {
        $curl = curl_init();
        curl_setopt(
            $curl, 
            CURLOPT_URL, 
            'http://api.myatompark.com/members/sms/xml.php'
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt(
            $curl, 
            CURLOPT_POSTFIELDS, 
            $this->createMessage($code, $phoneNumber)
        );
        $output = curl_exec($curl);
        curl_close($curl);
        return $output;
    }
    
    /**
     * Function that initiate connection with sms port and sending of sms
     * 
     * @param  string $code        <p>Activation code</p>
     * @param  string $phoneNumber <p>Phone number of the user</p>
     * @return boolean             <p>True if sending was successful 
     *                                 or false if it wasn't</p>
     */
    public function send($code, $phoneNumber)
    {
        $output = $this->run($code, $phoneNumber);
        if ($output === false) {
            return false;
        } else {
            return true;
        }
    }

}


