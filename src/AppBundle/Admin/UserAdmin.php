<?php
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

/**
 * Class that is integrated with SonataAdminBudnle
 * and used for administration of Entity/User Class 
 *
 * @category AdminClass
 * @package   
 * @author    
 * @license  
 * @link     
 */
class UserAdmin extends AbstractAdmin
{
    /**
     * Function that builds form for create and edit actions 
     * of admin panel
     * 
     * @param  FormMapper $formMapper FormMapper object
     * @return void
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'username', 
                'text', 
                [
                    'label' => 'Номер телефона',
                    'required' => false,
                ]
            )->add(
                'email', 
                'email', 
                [
                    'label' => 'Email',
                    'required' => false,
                ]
            )->add(
                'plainPassword', 
                'password', 
                [
                    'label' => 'Пароль',
                    'required' => false,
                ]
            )->add(
                'enabled', 
                'choice', 
                [
                    'label' => 'Статус',
                    'required' => false,
                    'choices' => [
                        'Заблокировать' => '0',
                        'Активировать' => '1',
                    ],
                ]
            )->add(
                'roles', 
                'choice', 
                [
                    'choices'  => self::getRolesForEdit(),
                    'required' => false,
                    'multiple' => true, 
                    'label' => 'Роли'
                ]
            );
    }

    /**
     * Function that configure filters
     * 
     * @param  DatagridMapper $datagridMapper DatagridMapper object
     * @return void
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('username');
    }

    /**
     * Function that configure the table with list of object 
     * at admin panel
     * 
     * @param  ListMapper $listMapper ListMapper object
     * @return void
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username', null, ['label' => 'Телефон'])
            ->addIdentifier('email')
            ->add('name', null, ['label' => 'Имя'])
            ->add(
                'roles', 
                'choice', 
                [
                    'choices'  => self::getRolesForLIst(),
                    'multiple' => true, 
                    'label' => 'Роли'
                ]
            )->add(
                '_action', 
                null, 
                [
                    'actions' => [
                        'show' => [],
                        'edit' => [],
                        'delete' => [],
                    ],
                    'label' => 'Действия'
                ]
            );
    }
    
    /**
     * Function that configure the show action
     * of admin panel
     * 
     * @param  ShowMapper $showMapper ShowMapper object
     * @return void
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('username', null, ['label' => 'Телефон'])
            ->add('email')
            ->add('name', null, ['label' =>'Имя'])
            ->add('surname', null, ['label' =>'Фамилия'])
            ->add('country', null, ['label' =>'Страна'])
            ->add('sex', null, ['label' =>'Пол'])
            ->add('birthday', 'date', ['label' =>'Дата рождения'])
            ->add('last_login', 'date', ['label' =>'Последний вход'])
            ->add(
                'messages', 
                'entity', 
                [
                    'class' => 'AppBundle\Entity\SystemMessage',
                    'label' => 'Сообщения',
                ]
            );
    }
    
    /**
     * Function that returns collection of roles for list action
     * 
     * @return array $roles <p>Collection of roles for $listMapper</p>
     */
    protected static function getRolesForLIst() 
    {
        $roles = [
            "ROLE_USER" => "Пользователь",
            "ROLE_SUPER_ADMIN" => "Главный администратор",
            "ROLE_SUPPORT" => "Помощник администратора",
            "ROLE_ADMIN" => "Администратор",
        ];
        return $roles;
    }
     
    /**
     * Function that returns collection of roles for edit and create actions
     * 
     * @return array $roles <p>Collection of roles for $formMapper</p>
     */
    protected static function getRolesForEdit() 
    {
        $roles = [
            "Пользователь" => "ROLE_USER",
            "Главный администратор" => "ROLE_SUPER_ADMIN",
            "Помощник администратора" => "ROLE_SUPPORT",
            "Администратор" => "ROLE_ADMIN",
        ];
        return $roles;
    }

    public function prePersist($object)
    {
        parent::prePersist($object);
    }

    public function preUpdate($object)
    {
        parent::preUpdate($object);
    }
}