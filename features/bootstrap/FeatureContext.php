<?php

use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class related to Contest category and used for test with behat
 *
 * @category Context
 * @package   
 * @author    
 * @license  
 * @link     
 */
class FeatureContext extends MinkContext implements KernelAwareContext
{
    private $kernel;
    
    private $session;
    
    public function __construct(Session $session) 
    {
        $this->session = $session;
    }
    
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * Function that used to reset the user data, that was changed
     * during the test editing of user information
     * 
     * @Then user :username have to change name on :name and surname on :surname
     */
    public function userHaveToChangeNameOnAndSurnameOn($username, $name, $surname) 
    {
        $userManager = $this->getContainer()->get('fos_user.user_manager');
        $user = $userManager->findUserByUsername($username);
        $user->setName($name);
        $user->setSurname($surname);
        $userManager->updateUser($user);
        return true;
    }
    
    /**
     * Function that modeled entering of 
     * activation code during registration
     * 
     * @Then I enter activation code in the field :field
     */
    public function iEnterActivationCodeInTheField($field) 
    {
        $session = $this->getContainer()->get('session');
        $this->fillField($field, $session->get('code'));
        return true;
    }
       
    /**
     * Function that deletes test user that was created 
     * for registration test by username
     * 
     * @Then I delete user with username :username
     */
    public function iDeleteUserWithUsername($username) 
    {
        $userManager = $this->getContainer()->get('fos_user.user_manager');
        $user = $userManager->findUserByUsername($username);
        $userManager->deleteUser($user);
        return true;
    }
    
    /**
     * Function that deletes test user that was created 
     * for registration test by email
     * 
     * @Then I delete user with email :email
     */
    public function iDeleteUserWithEmail($email) 
    {
        $userManager = $this->getContainer()->get('fos_user.user_manager');
        $user = $userManager->findUserByEmail($email);
        $userManager->deleteUser($user);
        return true;
    }
    /**
     * @Given /^I wait for (\d+) seconds$/
     */
    public function iWaitForSeconds($seconds)
    {
        $this->getSession()->wait($seconds * 1000);
    }
}