Feature: Login as Support and see, where we'll be

Scenario: login with user Support role
    Given I am on "/login"
    When I fill in "_username" with "a@aa.aa"
    When I fill in "_password" with "aa"
    When I press "_submit"
    Then I should be on "/admin/dashboard"