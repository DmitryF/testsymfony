Feature: Login as user and see, where we'll be

Scenario: login with user User role
    Given I am on "/login"
    When I fill in "_username" with "111111"
    When I fill in "_password" with "11111"
    When I press "_submit"
    Then I should be on "/profile/show"
    Then I should see "Личный кабинет"
    When I follow "Выход"
    Then I should be on "/login"